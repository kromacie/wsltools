# First Script
### Generate ED25519 Keys
Before you will do anything, just generate the ed25519 keys using the command:
```
alg=ed25519 && [ -f ~/.ssh/id_$alg.pub ] || ssh-keygen -t $alg -N '' -f ~/.ssh/id_$alg <<<y && cat ~/.ssh/id_$alg.pub
```

### Enabling Bridging
For WSL2 to work properly with multiple services, you need to enable bridging. Just follow this article and it will work for you
https://github.com/luxzg/WSL2-fixes/blob/master/networkingMode%3Dbridged.md


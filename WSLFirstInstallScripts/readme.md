# Usage
Once you install the WSL for the first time, just execute this script. It will:

- Install `ed25519` key
- Update the distro
- Install the jetbrains toolbox
- Install FUSE 2 dependency required for GUI apps in WSL2
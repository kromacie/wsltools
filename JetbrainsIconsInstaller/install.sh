#!/bin/bash

homedir=$(getent passwd "$USER" | cut -d: -f6)
user=$USER

title_case() { set "${*,,}" ; echo "${*^}" ; }

# todo it doesn't work for pycharm
for dir in "$homedir"/.local/share/JetBrains/Toolbox/apps/*/
do
    dirname=$(basename "$dir")
    app_name=$(title_case "${dirname//-/ }")

    sudo bash -c "sed \
      -e 's/\$APPNAME/$app_name/g' \
      -e 's/\$APP/$dirname/g' \
      -e 's/\$USER/$user/g' \
      _app.desktop > /usr/share/applications/$dirname.desktop"
done

cp ./toolbox.png /home/"$user"/.local/share/JetBrains/Toolbox/jetbrains-toolbox.png

sudo bash -c "sed \
  -e 's/\$USER/$user/g' \
  _toolbox.desktop > /usr/share/applications/toolbox.desktop"

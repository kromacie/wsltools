#!/bin/bash

sudo mkdir -p /etc/apt/keyrings
sudo curl -L -o /etc/apt/keyrings/syncthing-archive-keyring.gpg https://syncthing.net/release-key.gpg
echo "deb [signed-by=/etc/apt/keyrings/syncthing-archive-keyring.gpg] https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
echo "deb [signed-by=/etc/apt/keyrings/syncthing-archive-keyring.gpg] https://apt.syncthing.net/ syncthing candidate" | sudo tee /etc/apt/sources.list.d/syncthing.list

sudo apt update
sudo apt install syncthing supervisor -y

user=$USER

sudo bash -c "sed \
  -e 's/\$USER/$user/g' \
  syncthing.conf > /etc/supervisor/conf.d/syncthing.conf"

sudo supervisorctl reload